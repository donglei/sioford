module utils.memory;
import std.stdio;
import core.memory;
import std.conv;


T * makeNew(T)(T rhs)
{
    static if (is(T == class)) {
        static assert(false); // not implemented

    } else {
        T * p = cast(T*)GC.calloc(T.sizeof);
        emplace!T(p, rhs);
        return p;
    }
}

void* protect(void* obj) {
	GC.addRoot(obj);
	// Also ensure that a moving collector does not relocate
	// the object.
	GC.setAttr(obj, GC.BlkAttr.NO_MOVE);
	return obj;
}

void* release(void* obj) {
	GC.removeRoot(obj);			
    GC.clrAttr(obj, GC.BlkAttr.NO_MOVE);
    return obj;
}
