﻿module utils.Log;
import std.stdio;

void log(string msg, string file = __FILE__, string func = __PRETTY_FUNCTION__, int line= __LINE__) {
    debug {
        writefln("[%s][%s:%d]%s", func, file, line, msg);
        stdout.flush();
    }
}

