module utils.windowsapi;
version(Windows) :

private import core.sys.windows.windows;
private import std.c.windows.winsock, std.c.windows.com;

extern(Windows):

enum WSA_IO_PENDING = 997;

enum {
    TP_ELEMENT_MEMORY   = 1,
    TP_ELEMENT_FILE = 2,
    TP_ELEMENT_EOP    = 4
}

enum {
    TF_DISCONNECT  = 0x01,
    TF_REUSE_SOCKET  = 0x02,
    TF_WRITE_BEHIND = 0x04,
    TF_USE_DEFAULT_WORKER = 0x00,
    TF_USE_SYSTEM_THREAD = 0x10,
    TF_USE_KERNEL_APC = 0x20
}

struct TRANSMIT_PACKETS_ELEMENT {
  ULONG dwElFlags;
  ULONG cLength;
  union {
    struct {
      LARGE_INTEGER nFileOffset;
      HANDLE        hFile;
    };
    PVOID  pBuffer;
  };
}

alias TRANSMIT_PACKETS_ELEMENT* LPTRANSMIT_PACKETS_ELEMENT;
HANDLE CreateIoCompletionPort(HANDLE FileHandle, HANDLE ExistingCompletionPort, ULONG_PTR CompletionKey, DWORD NumberOfConcurrentThreads);
BOOL GetQueuedCompletionStatus(HANDLE CompletionPort, LPDWORD lpNumberOfBytes, PULONG_PTR lpCompletionKey, OVERLAPPED** lpOverlapped, DWORD dwMilliseconds);
BOOL PostQueuedCompletionStatus(HANDLE CompletionPort, DWORD dwNumberOfBytesTransferred, ULONG_PTR dwCompletionKey, OVERLAPPED* lpOverlapped);

alias BOOL function(SOCKET, SOCKET, PVOID, DWORD, DWORD, DWORD, LPDWORD, OVERLAPPED*) LPFN_ACCEPTEX;
const GUID WSAID_ACCEPTEX = {0xb5367df1,0xcbac,0x11cf,[0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92]};

alias BOOL function(SOCKET, sockaddr*, int, PVOID, DWORD, DWORD, OVERLAPPED*) LPFN_CONNECTEX;
const GUID WSAID_CONNECTEX = {0x25a207b9,0xddf3,0x4660,[0x8e,0xe9,0x76,0xe5,0x8c,0x74,0x06,0x3e]};

alias BOOL function(SOCKET, LPTRANSMIT_PACKETS_ELEMENT, DWORD, DWORD, OVERLAPPED*, DWORD) LPFN_TRANSMITPACKETS;
const GUID WSAID_TRANSMITPACKETS = {0xd9689da0,0x1f90,0x11d3,[0x99,0x71,0x00,0xc0,0x4f,0x68,0xc8,0x76]};

const int IOC_WS2 = 0x08000000;

enum : DWORD {
    IOCPARAM_MASK = 0x7f,
    IOC_VOID      = 0x20000000,
    IOC_OUT       = 0x40000000,
    IOC_IN        = 0x80000000,
    IOC_INOUT     = IOC_IN|IOC_OUT
}

template _WSAIORW(int x, int y) { const int _WSAIORW = IOC_INOUT | x | y; }
enum SIO_GET_EXTENSION_FUNCTION_POINTER = _WSAIORW!(IOC_WS2,6);

enum SO_UPDATE_ACCEPT_CONTEXT = 0x700B;

LPFN_ACCEPTEX getAcceptExFunction(SOCKET listenSocket) {
    import std.conv, std.exception;
    LPFN_ACCEPTEX fnAcceptEx;
    GUID GuidAcceptEx = WSAID_ACCEPTEX;
    DWORD dwBytes;
    auto iResult = WSAIoctl(listenSocket, SIO_GET_EXTENSION_FUNCTION_POINTER,
         &GuidAcceptEx, GuidAcceptEx.sizeof,
         &fnAcceptEx, LPFN_ACCEPTEX.sizeof,
         &dwBytes, null, null);
    enforce(iResult != SOCKET_ERROR, "WSAIoctl failed with error: " ~ to!string(WSAGetLastError()));
    return fnAcceptEx;
}

LPFN_TRANSMITPACKETS getTransmitPacketsFunction(SOCKET listenSocket) {
    import std.conv, std.exception;
    LPFN_TRANSMITPACKETS fn;
    GUID GuidTransmitPackets = WSAID_TRANSMITPACKETS;
    DWORD dwBytes;
    auto iResult = WSAIoctl(listenSocket, SIO_GET_EXTENSION_FUNCTION_POINTER,
         &GuidTransmitPackets, GuidTransmitPackets.sizeof,
         &fn, LPFN_TRANSMITPACKETS.sizeof,
         &dwBytes, null, null);
    enforce(iResult != SOCKET_ERROR, "WSAIoctl failed with error: " ~ to!string(WSAGetLastError()));
    return fn;
}

enum {
	WSAECONNRESET = 10054,
	WSAEDISCON = 10101
}



struct WSABUF {
    uint  len;
    char* buf;
}

alias WSABUF* LPWSABUF;
alias typeof(&WSASendTo) LPFN_WSASENDTO;
alias HANDLE WSAEVENT;

int WSASendTo(SOCKET, LPWSABUF, DWORD, LPDWORD, DWORD, sockaddr*, DWORD, OVERLAPPED*, LPWSAOVERLAPPED_COMPLETION_ROUTINE);
        BOOL WSASetEvent(WSAEVENT);

int WSARecvFrom(SOCKET, LPWSABUF, DWORD, DWORD*, DWORD*, sockaddr*, DWORD*, OVERLAPPED*, LPWSAOVERLAPPED_COMPLETION_ROUTINE);
