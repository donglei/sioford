module utils.container;

import std.algorithm;

class Set(E) {
    private E[] elements;

    void add(ref E element) {
        elements ~= element;
    }

    void remove(ref E element) {
        foreach (i; 0.. elements.length) {
            if (elements[i] is element) {
                elements = elements.remove(i);
                break;
            }
        }
    }

    void forEach(void delegate (ref E element) func) {
        foreach(E value; elements) {
            func(value);
        }
    }
}
