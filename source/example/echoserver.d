﻿module example.echoserver;

import io.socket, io.serversocket, io.message;
import devent.eventqueue;
import std.conv, std.stdio, std.string, std.exception, std.algorithm;

class TCPEchoSocketStateHandler : TCPSocketStateHandler {
    
    this(TCPSocket socket, ref EventQueue eventQueue) {
        super(socket, eventQueue);
    }
    
    override SocketState readyToRead() {
        currentIndex = currentIndex >= buffer.length ? 0 : currentIndex;
        auto readBuffer = buffer[currentIndex..$];
        auto message = socket.recvMessage(readBuffer);
        if (message.size > 0) {
            auto workingBuffer = buffer[currentIndex..$];
            auto lineEnd = countUntil!("a == b", char[], char)(workingBuffer, '\n');
            currentIndex += message.size;
            if(lineEnd > -1) {
                writeBuffer = workingBuffer[0..lineEnd + 1];
                return SocketState.WantWrite;
            }
            if(currentIndex >= buffer.length) {
                writeBuffer = workingBuffer;
                return SocketState.WantWrite;
            }
        }
        return SocketState.WantRead;
    }
    
    override SocketState readyToWrite() {
        write("Client-->" ~to!string(writeBuffer));
        stdout.flush();
        auto msg = new IOMessage(writeBuffer);
        socket.sendMessage(msg);
        return SocketState.WantRead;
    }
    
    override void eof() {
        writeln("Connection from " ~ socket.remoteAddress.toString() ~ " ended");
    }
    
    override SocketState error(int err) {
        writeln("Connection from " ~ socket.remoteAddress.toString() ~ " errored with code " ~ to!string(err));
        return SocketState.WantClose;
    }
    
    override SocketState startState() {
        return SocketState.WantRead;
    }
    
private:
    char[10] buffer;
    char[] writeBuffer;
    uint currentIndex = 0;
    uint writeStart = 0;
    
}


class UDPEchoSocketStateHandler : UDPSocketStateHandler {
    
    this(UDPSocket socket, ref EventQueue eventQueue) {
        super(socket, eventQueue);
    }
    
    override SocketState readyToRead() {
        auto message = socket.recvMessage();
        auto buffer = message.msgBody;
        if(message.size > 0) {
            auto indexEnd = countUntil!("a == b", char[], char)(buffer, '\n');
            zbuff ~= buffer;
            toAddress = message.fromAddress;
            if (message.isShortRead) {
                writeln("Short read from client " ~ to!string(message.fromAddress));
                return SocketState.WantWrite;
            } else if(indexEnd > -1) {
                return SocketState.WantWrite;
            }
        }
        return SocketState.WantRead;
    }

    override SocketState readyToWrite() {
        writeln("Client--->" ~ to!string(zbuff));
        stdout.flush();
        auto msg = new IOMessage(zbuff, toAddress);
        socket.sendMessage(msg, true);
        zbuff.length = 0;
        return SocketState.WantRead;
    }
    
    override SocketState startState() {
        return startStateConf;
    }
    
    override SocketState error(int err) {
        writeln("Connection errored with code " ~ to!string(err));
        return SocketState.WantRead;
    }
    
private:
    char[] zbuff;
    Address toAddress;
    SocketState startStateConf = SocketState.WantRead;
}


