module io.socket;

import std.exception, std.conv, std.stdio;
import utils.memory, io.message;
import devent.eventqueue, devent.event;
import utils.Log;

public import std.socket : AddressFamily, ProtocolType, SocketType, Address,
    SocketOSException, InternetAddress, Internet6Address, UnknownAddress,
    SocketParameterException, SocketFlags;

version(Windows)
{
    pragma (lib, "ws2_32");

    shared static this() {
        WSADATA wd;

        // Winsock will still load if an older version is present.
        // The version is just a request.
        int val;
        val = WSAStartup(0x2020, &wd);
        if(val)         // Request Winsock 2.2 for IPv6.
            throw new SocketOSException("Unable to initialize socket library", val);
    }

    private import utils.windowsapi;
    private import std.c.windows.windows, std.c.windows.winsock, std.windows.syserror;
    private alias std.c.windows.winsock.timeval _ctimeval;
    private alias std.c.windows.winsock.linger _clinger;

    enum socket_t : SOCKET { INVALID_SOCKET }
    protected const int _SOCKET_ERROR = SOCKET_ERROR;
    int lastErr()
    {
        return WSAGetLastError();
    }

    enum SOCKET_ACCEPT_ADDRESS_LENGTH = sockaddr_in.sizeof + 16;
}
else version(Posix)
{
    version(linux)
        import std.c.linux.socket : AF_IPX, AF_APPLETALK, SOCK_RDM,
               IPPROTO_IGMP, IPPROTO_GGP, IPPROTO_PUP, IPPROTO_IDP,
               SD_RECEIVE, SD_SEND, SD_BOTH, MSG_NOSIGNAL, INADDR_NONE,
               TCP_KEEPIDLE, TCP_KEEPINTVL;
    else version(OSX)
        import std.c.osx.socket : AF_IPX, AF_APPLETALK, SOCK_RDM,
               IPPROTO_IGMP, IPPROTO_GGP, IPPROTO_PUP, IPPROTO_IDP,
               SD_RECEIVE, SD_SEND, SD_BOTH, INADDR_NONE;
    else version(FreeBSD)
    {
        import core.sys.posix.sys.socket;
        import core.sys.posix.sys.select;
        import std.c.freebsd.socket;
        private enum SD_RECEIVE = SHUT_RD;
        private enum SD_SEND    = SHUT_WR;
        private enum SD_BOTH    = SHUT_RDWR;
    }
    else
        static assert(false);

    import core.sys.posix.netdb;
    private import core.sys.posix.fcntl;
    private import core.sys.posix.unistd;
    private import core.sys.posix.arpa.inet;
    private import core.sys.posix.netinet.tcp;
    private import core.sys.posix.netinet.in_;
    private import core.sys.posix.sys.time;
    private import core.sys.posix.sys.socket;
    private alias core.sys.posix.sys.time.timeval _ctimeval;
    private alias core.sys.posix.sys.socket.linger _clinger;

    private import core.stdc.errno;

    enum socket_t : int32_t { init = -1 }
    protected const int _SOCKET_ERROR = -1;


    int lastErr()
    {
        return errno;
    }
}
else
{
    static assert(0);     // No socket support yet.
}

enum SocketState {
    WantRead,
    WantWrite,
    WantClose,
    ConnClosed
}

// Handlers used to maintain socket state
// Called by the actual SocketEventHandler set up for the queue
abstract class SocketStateHandler {

    SocketState readyToRead();

    SocketState readyToWrite();

    /** State to enter when the socket is started/connected **/
    SocketState startState();

    SocketState error(int code);

    @property Socket socket() {
        return null;
    }

    this(ref EventQueue eventQueue) {
        this.mEventQueue = eventQueue;
    }

    void forceShutdown() {
        socket.close();
    }

    final void handleEvent(Event event) {
        if(!handleCustomEvent(event)) {
            switch(event.flag) {
                case EventFlag.READ :
                    version(Windows) {
                        transitionState(readyToRead());
                    } else version(Posix) {
                        transitionState(event, readyToRead());
                    }
                    break;
                case EventFlag.WRITE :
                    version(Windows) {
                        transitionState(readyToWrite());
                    } else version(Posix) {
                        transitionState(event, readyToWrite());
                    }
                    break;
                case EventFlag.ERROR :
                    version(Windows) {
                        transitionState(error(lastErr()));
                    } else version(Posix) {
                        transitionState(event, error(lastErr()));
                    }
                    break;
                default :  throw new Exception("Unhandled Event " ~ to!string(event.flag));
            }
        }
    }

    void prepareForIO() {
        version(Windows) {
            mEventQueue.registerEventSource(socket.socketHandle);
            auto ev = new devent.event.Event(Variant(this), EventFilter.READ);
            socket.readEvent = mEventQueue.createQueueSpecificEvent(ev);
            ev = new devent.event.Event(Variant(this), EventFilter.WRITE);
            socket.writeEvent = mEventQueue.createQueueSpecificEvent(ev);
        } else version(Posix) {
            if(startState() == SocketState.WantWrite) {
                registerForWrite();
            } else {
                registerForRead();
            }
        }

    }

protected:

    // Allow custom event handler to modify event if required hence ref
    bool handleCustomEvent(ref Event event);

    version(Posix) {
        void transitionState(ref Event source, SocketState state) {
            // In FreeBSD/Linux we need to swap read write states
            if(state == SocketState.WantWrite && source.eventFilter == EventFilter.READ) {
                registerForWrite();
            } else if(state == SocketState.WantRead && source.eventFilter == EventFilter.WRITE) {
                registerForRead();
            } else if(state == SocketState.WantClose) {
                socket.close();
            }
        }
        private void registerForWrite() {
            if (mCurrentState.hasValue && mCurrentState.get!(EventFilter) != EventFilter.WRITE) {
                auto localState = mCurrentState.get!(EventFilter);
                mEventQueue.deregisterEventSource(socket.socketHandle, localState);
            }

            if ((!mCurrentState.hasValue) || mCurrentState.get!(EventFilter) != EventFilter.WRITE) {
                auto writeEvent = new devent.event.Event(Variant(this), EventFilter.WRITE);
                mEventQueue.registerEventSource(socket.socketHandle, writeEvent);
                mCurrentState = EventFilter.WRITE;
            }
        }
        private void registerForRead() {
            if (mCurrentState.hasValue && mCurrentState.get!(EventFilter) != EventFilter.READ) {
                auto localState = mCurrentState.get!(EventFilter);
                mEventQueue.deregisterEventSource(socket.socketHandle, localState);
            }

            if ((!mCurrentState.hasValue) || mCurrentState.get!(EventFilter) != EventFilter.READ) {
                auto readEvent = new devent.event.Event(Variant(this), EventFilter.READ);
                mEventQueue.registerEventSource(socket.socketHandle, readEvent);
                mCurrentState = EventFilter.READ;
            }
        }
    }
    version(Windows) {
        void transitionState(SocketState state) {
            // Simulate events on the queue to allow EOF and Writes
            if(state == SocketState.WantClose) {
                auto eofEvent = new devent.event.Event(Variant(this), EventFilter.WRITE);
                eofEvent.flag = EventFlag.EOF;
                mEventQueue.triggerEvent(eofEvent);
            } else if(state == SocketState.WantWrite) {
                auto writeEvent = new devent.event.Event(Variant(this), EventFilter.WRITE);
                writeEvent.flag = EventFlag.WRITE;
                mEventQueue.triggerEvent(writeEvent);
            }  else if(state == SocketState.WantRead) {
                socket.queueRead();
            }
        }
    }

private:
    Variant mCurrentState;
    EventQueue mEventQueue;
}

abstract class SocketEventHandler : EventHandler {
    void handle(ref Event myEvent) {
        auto socketStateHandler = myEvent.payload.get!SocketStateHandler();
        socketStateHandler.handleEvent(myEvent);
    }
}

class BasicSocketEventHandler : SocketEventHandler {
    this(EventQueue eventQueue, void delegate() shutdown) {
		eventQueue.registerHandler(this);
    }

	void shutdown() {
		mShutdown();
	}

private:
	void delegate() mShutdown;
}


abstract class Socket {

private:
    AddressFamily mFamily;
    SocketType mType;
    ProtocolType mProtocol;
    socket_t mSocketHandle;

public:
    this(AddressFamily af, SocketType type, ProtocolType protocol) {
        auto sh = cast(socket_t) .socket(af, type, protocol);

        if(sh == socket_t.init) {
            throw new SocketOSException("Unable to create socket: " ~ to!string(lastErr()));
        }
        this(af, type, protocol, sh);
    }

    this(AddressFamily af, SocketType type, ProtocolType protocol, socket_t sh) {
        mFamily = af;
        mType = type;
        mProtocol = protocol;
        mSocketHandle = sh;
    }

    ~this() {
        close();
    }

    void close() {
        if(mSocketHandle != socket_t.init) {
            version(Windows) {
                .closesocket(mSocketHandle);
            }
            else version(Posix) {
                .close(mSocketHandle);
            }
        }
    }

    void withSocketHandle(void delegate (socket_t) handler) {
        handler(mSocketHandle);
    }

    @property {

        static string hostName() {
            char[256] result;         // Host names are limited to 255 chars.
            if(_SOCKET_ERROR == .gethostname(result.ptr, result.length))
                throw new SocketOSException("Unable to obtain host name");
            return to!string(result.ptr);
        }

        /// Local endpoint $(D Address).
        Address localAddress() {
            Address addr = createAddress();
            socklen_t nameLen = addr.nameLen;
            if(_SOCKET_ERROR == .getsockname(mSocketHandle, addr.name, &nameLen))
                throw new SocketOSException("Unable to obtain local socket address");
            if(nameLen > addr.nameLen)
                throw new SocketParameterException("Not enough socket address storage");
            assert(addr.addressFamily == mFamily, to!string(addr.addressFamily) ~ "!=" ~ to!string(mFamily));
            return addr;
        }

        auto socketHandle() {
            return mSocketHandle;
        }

    }

    void setNonBlocking() {
        version(Windows) {
            uint flag = 1;
            enforce(_SOCKET_ERROR != ioctlsocket(mSocketHandle, FIONBIO, &flag),
                new SocketOSException("Unable to set socket to non-blocking", lastErr()));
        } else version(Posix) {
            int x = fcntl(mSocketHandle, F_GETFL, 0);
            enforce(-1 != x, new SocketOSException("Unable to set socket to non-blocking", lastErr()));
            x |= O_NONBLOCK;
            enforce(-1 != fcntl(mSocketHandle, F_SETFL, x),
                new SocketOSException("Unable to set socket to non-blocking", lastErr()));
        }
    }

protected:

    void bind(Address addr, int backlog) {
        enforce(_SOCKET_ERROR != .bind(socketHandle, addr.name, addr.nameLen), new SocketOSException("Unable to bind socket"));
    }

    @property {

        auto family() {
            return mFamily;
        }

        auto type() {
            return mType;
        }

        auto protocol() {
            return mProtocol;
        }
    }

    Address createAddress() {
        Address result;
        switch(mFamily) {
        case AddressFamily.INET:
            result = new InternetAddress(InternetAddress.ADDR_ANY, InternetAddress.PORT_ANY);
            break;

        case AddressFamily.INET6:
            result = new Internet6Address(Internet6Address.ADDR_ANY, Internet6Address.PORT_ANY);
            break;

        default:
            result = new UnknownAddress;
        }
        return result;
    }

    Address createAddress(sockaddr* addr) {
        Address result;
        switch(addr.sa_family) {
            case AddressFamily.INET:
                sockaddr_in* conv = cast(sockaddr_in*)addr;
                result = new InternetAddress(ntohl(conv.sin_addr.s_addr), ntohs(conv.sin_port));
                break;

            case AddressFamily.INET6:
                sockaddr_in6* conv = cast(sockaddr_in6*)addr;
                result = new Internet6Address(conv.sin6_addr.s6_addr, ntohs(conv.sin6_port));
                break;

            default:
                result = new UnknownAddress;
        }
        return result;
    }

    version(Windows) {
        void queueRead() {
        }
    }


private:
    version(Windows) {
        OverlappedPayload* writeEvent;
        OverlappedPayload* readEvent;
        LPFN_TRANSMITPACKETS TransmitPackets = null;
    }

}

abstract class TCPSocketStateHandler : SocketStateHandler {

    /* notification that an eof event has occured. Do not close the socket
     * the framework will do that for you */
    void eof();

    override @property TCPSocket socket() {
        return mSocket;
    }

    version(Windows) {
        override void prepareForIO() {
            super.prepareForIO();
            transitionState(startState());
        }
    }

protected:
    override bool handleCustomEvent(ref Event event)
    {
        switch(event.flag)
        {
            case EventFlag.EOF :
                version (Posix)
                {
                    // allow handler to read from buffer as it might have data in it
                    transitionState(event, readyToRead());
                }
                eof();
                socket.close();
                break;
            case EventFlag.ERROR :
                version(Windows)
                {
                    int err = lastErr();
                    if(err == WSAECONNRESET || err == WSAEDISCON) {
                        eof();
                        socket.close();
                    } else {
                        transitionState(error(err));
                    }
                    break;
                }
                version(Posix)
                {
                    return false;
                }
            default :  return false;
        }
        return true;
    }

    this(TCPSocket socket, ref EventQueue eventQueue) {
        super(eventQueue);
        this.mSocket = socket;
    }

    version(Windows){
        override void transitionState(SocketState state) {
            if(socket.eof) {
                super.transitionState(SocketState.WantClose);
            } else {
                super.transitionState(state);
            }
        }
    }

private:
    TCPSocket mSocket;

}

class TCPSocket : Socket {
    version(Windows) {
        import std.c.windows.windows : BYTE, DWORD, HANDLE, OVERLAPPED, ReadFile;
    }

public:
    this(AddressFamily af) {
        super(af, SocketType.STREAM, ProtocolType.TCP);
    }

    this(AddressFamily af, ref EventQueue eventQueue) {
        super(af, SocketType.STREAM, ProtocolType.TCP);
    }


    this(AddressFamily af, socket_t sh) {
        super(af, SocketType.STREAM, ProtocolType.TCP, sh);
    }

    auto eof() {
        return mEOF;
    }

    /// Remote endpoint $(D Address).
    Address remoteAddress() {
        Address addr = createAddress();
        socklen_t nameLen = addr.nameLen;
        if(_SOCKET_ERROR == .getpeername(mSocketHandle, addr.name, &nameLen))
            throw new SocketOSException("Unable to obtain remote socket address");
        if(nameLen > addr.nameLen)
            throw new SocketParameterException("Not enough socket address storage");
        assert(addr.addressFamily == mFamily, to!string(addr.addressFamily) ~ "!=" ~ to!string(mFamily));
        return addr;
    }

    IOMessage recvMessage(ref char[] zeroCopyBuf) {
        enforce(zeroCopyBuf.length > 0, "Buffer must have a size of at least 1");
        auto ret = .recv(mSocketHandle, zeroCopyBuf.ptr, to!int(zeroCopyBuf.length), SocketFlags.NONE);
        version(Posix) {
            if(ret == _SOCKET_ERROR) {
                auto err = lastErr();
                switch(err) {
                    case ECONNRESET : mEOF = true;
                        break;
                    case EAGAIN:
                        break;
                    default: throw new SocketOSException("Read from socket failed", lastErr());
                }
            }
        }
        version(Windows) {
            enforce(_SOCKET_ERROR != ret,
                new SocketOSException("Read from socket failed", lastErr()));
            mEOF = ret == 0;
        }
        auto refBuf = zeroCopyBuf[0..ret];
        return new IOMessage(refBuf, remoteAddress, localAddress);
    }

    size_t sendMessage(IOMessage message) {
        if(message.msgBody.length > 0) {
            version(Posix) {
                auto ret = .send(mSocketHandle, message.msgBody.ptr, to!int(message.msgBody.length), message.flags);
                if(ret == _SOCKET_ERROR) {
                    if(lastErr() == ECONNRESET) {
                        mEOF = true;
                    } else {
                        throw new SocketOSException("Send on socket failed", lastErr());
                    }
                }
            }
            version(Windows) {
                if(TransmitPackets == null) {
                    TransmitPackets = getTransmitPacketsFunction(this.socketHandle);
                }
                auto transmitPackets = makeNew(TRANSMIT_PACKETS_ELEMENT());
                transmitPackets.dwElFlags = TP_ELEMENT_MEMORY;
                transmitPackets.cLength = message.msgBody.length;
                transmitPackets.pBuffer = message.msgBody.ptr;
                auto ret = TransmitPackets(this.socketHandle, transmitPackets, 1,0, null, TF_USE_DEFAULT_WORKER);
                if(!ret) {
                    auto err = lastErr();
                    auto mEOF = err == WSAECONNRESET;
                    enforce(mEOF || err == WSA_IO_PENDING,
                        new SocketOSException("Write to socket " ~to!string(this.socketHandle) ~ " failed", err));
                }
            }
            return ret;
        } else {
            return 0;
        }
    }

    void connect(Address addr) {
        enforce(_SOCKET_ERROR != .connect(socketHandle, addr.name, addr.nameLen), new SocketOSException("Unable to connect to " ~ addr.toAddrString));
    }

protected:
    version(Windows) {
        override void queueRead() {
            auto ret = ReadFile(cast(HANDLE)socketHandle, cast(void*)null, 0, null, cast(OVERLAPPED*)readEvent);
            auto code = lastErr();
            enforce(ret || code == WSA_IO_PENDING, "Failed to prepare socket for reading:"  ~ to!string(code));
        }
    }
private:
    bool mEOF = false;
}

abstract class UDPSocketStateHandler : SocketStateHandler {

    this(UDPSocket socket, ref EventQueue eventQueue) {
        super(eventQueue);
        this.mSocket = socket;
    }

    override @property UDPSocket socket() {
        return mSocket;
    }

    version(Windows) {
        override void prepareForIO() {
            super.prepareForIO();
            auto sState = startState();
            if(sState == SocketState.WantRead) {
                enforce(socket.isBound, "Socket must be bound to read");
            }
            transitionState(sState);
        }
    }

protected:
    override bool handleCustomEvent(ref Event event) {
        version(Windows) {
            if(event.flag == EventFlag.ERROR) {
                int err = lastErr();
                if(err == WSAECONNRESET || err == WSAEDISCON) {
                    socket.close();
                    return true;
                } else if(err == ERROR_MORE_DATA) {
                    // incomplete read, remaining data would have been discarded
                    writeln("----->>>>>Incomplete read: " ~ to!string(event));
                    event.flag = EventFlag.READ;
                    socket.lastReadShort = true;
                    return false;
                }
            } else if(event.flag == EventFlag.EOF) {
                // in windows we post an EOF event on the queue so we have to act on it
                socket.close();
                return true;
            }
        }
        return false;
    }
private:
    UDPSocket mSocket;
}

uint MTU = 256; // 512 actual data available, 576 actual packet size best for Internet, 1500 for eth. Best to determine this at runtime

abstract class BufferManager {
    public char[] getNextBuffer();
}

class DefaultBufferManager : BufferManager {

    this(uint bufferSize = MTU) {
        this.bufferSize = bufferSize;
    }

    public override char[] getNextBuffer() {
        return new char[bufferSize];
    }

private:
    uint bufferSize;
}

class UDPSocket : Socket {

    version(Windows) {
        import std.c.windows.windows : BYTE, DWORD, HANDLE, OVERLAPPED, ReadFile;
    }

public:
    this(AddressFamily af, InternetAddress bindAddr) {
        this(af, bindAddr, new DefaultBufferManager());
    }

    this(AddressFamily af, InternetAddress bindAddr, BufferManager bm)
    in {
        assert(bindAddr !is null);
    } body {
        super(af, SocketType.DGRAM, ProtocolType.UDP);
        bind(bindAddr, 10);
        version(Windows) {
			bound = true;
		}
        this.bufferManager = bm;
    }
    // get mtu function
    // Use ioctl SIOCGIFMTU on posix
    // use getsockopt SOL_SOCKET, SO_MAX_MSG_SIZE on windows
    this(AddressFamily af) {
        this(af, new DefaultBufferManager());
    }

    this(AddressFamily af, BufferManager bm) {
        super(af, SocketType.DGRAM, ProtocolType.UDP);
        this.bufferManager = bm;
    }

    this(AddressFamily af, socket_t sh) {
        this(af, sh, new DefaultBufferManager());
    }
    this(AddressFamily af, socket_t sh, BufferManager bm) {
        super(af, SocketType.DGRAM, ProtocolType.UDP, sh);
        this.bufferManager = bm;
    }

    version(Posix) {
        IOMessage recvMessage() {
            auto zeroCopyBuf = bufferManager.getNextBuffer();
            import core.sys.posix.sys.uio;
            sockaddr_storage srcAddr;
            iovec iov;
            iov.iov_base=zeroCopyBuf.ptr;
            iov.iov_len=to!int(zeroCopyBuf.length);
            
            msghdr message;
            message.msg_name=&srcAddr;
            message.msg_namelen=srcAddr.sizeof;
            message.msg_iov=[iov].ptr;
            message.msg_iovlen=1;
            message.msg_control=null;
            message.msg_controllen=0;

            auto ret = .recvmsg(mSocketHandle, &message, 0);
            if(ret == _SOCKET_ERROR) {
                throw new SocketOSException("Read from socket failed", lastErr());
            }
            Address remoteAddress = createAddress(cast(sockaddr*) &srcAddr);
            auto smallBuf = zeroCopyBuf[0..ret];
            if(message.msg_flags & MSG_TRUNC) {
                return new IOMessage(smallBuf, localAddress, remoteAddress, true);
            } else {
                return new IOMessage(smallBuf, localAddress, remoteAddress);
            }
        }
    }
    version(Windows) {
    
        IOMessage recvMessage() {
            // a queued read should have completed for the caller to be using this method
            // TODO check for buffer too small errors here otherwise data will silently get dropped
            char[] refBuf;
            if (readEvent.numBytes < queuedBufferArray.length) {
                refBuf = queuedBufferArray[0..readEvent.numBytes];
            } else {
                refBuf = queuedBufferArray[0..$];
            }
            auto senderAddress = createAddress(&senderAddr);
            return new IOMessage(refBuf, localAddress, senderAddress, lastReadShort);
        }


    }

    size_t sendMessage(IOMessage inMessage, bool allowFragmentation = false)
    in
    {
        assert(inMessage.toAddress !is null);
    }
    body
    {
        IOMessage[] msgs;
        if (allowFragmentation) {
            msgs = fragmentMessage(MTU, inMessage);
        } else {
            enforce(inMessage.msgBody.length <= MTU, "Message body cannot exceed " ~ to!string(MTU) ~" bytes");
            msgs = [inMessage];
        }

        size_t sentCount = 0;
        foreach(IOMessage message; msgs) {
            if(message.msgBody.length > 0) {
                version(Posix) {
                    auto ret = .sendto(mSocketHandle, message.msgBody.ptr, to!int(message.msgBody.length), message.flags,
                        message.toAddress.name, message.toAddress.nameLen);
                    if(ret == _SOCKET_ERROR) {
                        throw new SocketOSException("Send on socket failed", lastErr());
                    }
                }
                version(Windows) {
                    DWORD numBytes;
                    auto wsaBuf = WSABUF(message.msgBody.length, message.msgBody.ptr);
                    auto ret = WSASendTo(mSocketHandle, &wsaBuf, 1, &numBytes, 0, message.toAddress.name, message.toAddress.nameLen,
                        //cast(OVERLAPPED*)writeEvent, null);
                                         null, null);
                    if(ret == _SOCKET_ERROR) {
                        auto err = lastErr();
                        enforce(err == WSA_IO_PENDING,
                            new SocketOSException("Write to socket " ~to!string(this.socketHandle) ~ " failed", err));
                    }
                }
                sentCount +=ret;
            }
        }
        return sentCount;
    }

protected:
    version(Windows) {
        override void queueRead() {
            queuedBufferArray = bufferManager.getNextBuffer();
            WSABUF queuedReadBuffer = WSABUF(queuedBufferArray.length, queuedBufferArray.ptr);
            lastReadShort = false;
            DWORD flags = 0;
            DWORD numBytesRead;
            auto ret = WSARecvFrom(mSocketHandle, &queuedReadBuffer, 1, &numBytesRead, &flags, &senderAddr, &senderAddressSize,
                cast(OVERLAPPED*)readEvent, cast(LPWSAOVERLAPPED_COMPLETION_ROUTINE)null);
            auto code = lastErr();
            enforce(ret != SOCKET_ERROR || code == WSA_IO_PENDING, "Failed to prepare socket for reading:"  ~ to!string(code));
        }

        @property isBound() {
            return bound;
        }

        @property lastReadShort() {
            return mLastReadWasShort;
        }

        @property lastReadShort(bool value) {
            mLastReadWasShort = value;
        }

    }
private:
//        auto stateHandler = stateHandlerFactory(this);
//        mEventHandler = new BasicSocketEventHandler();
//        eventQueue.registerHandler(mEventHandler);
//        stateHandler.prepareForIO();
    version(Windows) {
        DWORD senderAddressSize = sockaddr.sizeof;
        sockaddr senderAddr;
        char[] queuedBufferArray;
        bool bound;
        bool mLastReadWasShort = false;
    }
    BufferManager bufferManager;
}
