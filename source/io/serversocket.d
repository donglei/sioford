module io.serversocket;

import std.exception, std.conv, std.stdio;
import io.socket;
import devent.eventqueue, devent.event;
import utils.windowsapi, utils.memory, utils.container;

alias void delegate(TCPSocket socket) TCPSocketBoundHandler;

version(Windows) {
    private import std.c.windows.winsock : listen, bind;
}

version(Posix) {
    private import core.sys.posix.sys.socket : listen, bind, accept;
}

alias TCPSocketStateHandler delegate(TCPSocket socket, ref EventQueue eventQueue) TCPSocketStateHandlerFactory;

class TCPServerSocket : Socket {

 public:

    this(AddressFamily af, ref EventQueue eventQueue, TCPSocketStateHandlerFactory factory, TCPSocketBoundHandler handler) {
        this.mSocketBoundHandler = handler;
        this.mStateFactory = factory;
		this.mEventQueue = eventQueue;
        super(af, SocketType.STREAM, ProtocolType.TCP);
        mSocketStateHandlers = new Set!TCPSocketStateHandler();
    }

    /**
     * We Combine bind and listen as there are no events generated from bind
     */
    override void bind(Address addr, int backlog) {
        super.bind(addr, backlog);
        mEventQueue.registerHandler(new BindEventHandler());
        version(Windows) {
            mEventQueue.registerEventSource(socketHandle);
            enforce(_SOCKET_ERROR != .listen(socketHandle, backlog),
                new SocketOSException("Unable to listen on socket"));
        } else version(Posix) {
            version (FreeBSD) {
                enforce(_SOCKET_ERROR != .listen(socketHandle, backlog),
                        new SocketOSException("Unable to listen on socket"));
            }
            auto stateHandler = new ServerStateHandler();
            auto event = new Event(Variant(stateHandler), EventFilter.READ);
            mEventQueue.registerEventSource(socketHandle, event);
            version(linux) {
                enforce(_SOCKET_ERROR != .listen(socketHandle, backlog),
                        new SocketOSException("Unable to listen on socket"));
            }
        }

        version(Windows) {
            auto as = new AcceptExSocket(family, mEventQueue, this);
            protect(cast(void*)as);
        }
    }

    class ServerStateHandler {

        version(Windows) {
            this(AcceptExSocket socket) {
                this.mSocket = socket;
            }
            private AcceptExSocket mSocket;
        }

        void connect() {
            version(Windows) {
                auto childSocket = mSocket;
                import std.c.windows.winsock;
                childSocket.withSocketHandle ( (socket_t sh) { sh.setsockopt(SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT,
                    cast(char*)&sh, SOCKET.sizeof);});
                auto as = new AcceptExSocket(family, mEventQueue, this.outer);
                protect(cast(void*)as);
            } else version(Posix) {
                auto childSocket = new PosixAcceptSocket(family, this.outer);
            }

            if(hasSocketBoundHandler) {
                mSocketBoundHandler(childSocket);
            }
            auto stateHandler = mStateFactory(childSocket, mEventQueue);
            mSocketStateHandlers.add(stateHandler);
            stateHandler.prepareForIO();
        }

    }
private:

    class BindEventHandler : SocketEventHandler {
        override void handle(ref Event myEvent) {
            if(myEvent.payload.type == typeid(ServerStateHandler)) {
                auto socketStateHandler = myEvent.payload.get!ServerStateHandler();
                enforce(myEvent.flag == EventFlag.READ, "Unable to handle " ~ to!string(myEvent.flag) ~ " events");
                socketStateHandler.connect();
            } else {
                super.handle(myEvent);
                if(myEvent.flag == EventFlag.EOF) {
                    auto socketStateHandler = cast(TCPSocketStateHandler)myEvent.payload.get!SocketStateHandler();
                    mSocketStateHandlers.remove(socketStateHandler);
                }
            }
        }
        void shutdown() {
            mSocketStateHandlers.forEach((ref h){ h.forceShutdown();});
            this.outer.close();
        }
    }

    @property bool hasSocketBoundHandler() {
        return mSocketBoundHandler != null;
    }

    TCPSocketBoundHandler mSocketBoundHandler;
    TCPSocketStateHandlerFactory mStateFactory;
    Set!(TCPSocketStateHandler) mSocketStateHandlers;
	EventQueue mEventQueue;
}

// These sockets need to be non blocking because of the buffer size issue. TCP being streaming could have a message size larger
// than the buffer provided in TCPSocket.recvMessage
version(Windows) {
    import std.c.windows.windows : BYTE, DWORD, HANDLE, OVERLAPPED, ReadFile;

    class AcceptExSocket : TCPSocket {
    public:
        this(AddressFamily af, ref EventQueue eventQueue, TCPServerSocket listenerSocket) {
            super(af);
            auto AcceptEx = getAcceptExFunction(listenerSocket.socketHandle);
            auto stateHandler = listenerSocket.new ServerStateHandler(this);
            auto ev = new devent.event.Event(Variant(stateHandler), EventFilter.READ);
            auto event = eventQueue.createQueueSpecificEvent(ev);
            auto ret = AcceptEx(listenerSocket.socketHandle, socketHandle, &myAddrBlock, 0,
                SOCKET_ACCEPT_ADDRESS_LENGTH, SOCKET_ACCEPT_ADDRESS_LENGTH, &receiveLen, cast(OVERLAPPED*)event);
            auto code = lastErr();
            enforce(ret == false && code == 997, to!string(code));
        }
    private:
        BYTE myAddrBlock[SOCKET_ACCEPT_ADDRESS_LENGTH * 2];
        DWORD receiveLen;
    }
}

version(Posix) {

    class PosixAcceptSocket : TCPSocket {
        this(AddressFamily af, TCPServerSocket listenerSocket) {
            auto acHandle = cast(socket_t).accept(listenerSocket.socketHandle, null, null);
            enforce(acHandle > 0, new SocketOSException("Accept failed: " ~ to!string(lastErr())));
            super(af, acHandle);
        }
    }
}
