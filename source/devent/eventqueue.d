module devent.eventqueue;

import devent.ieventqueue;
import devent.exithandler;
public import devent.ieventqueue : EventHandler;
import std.exception, std.variant, std.conv, std.stdio;
import core.memory, core.stdc.errno, core.stdc.string;
import utils.memory;

version(Windows) {
    private import utils.windowsapi, std.c.windows.windows;

    @property DWORD lastError() {
        return GetLastError();
    }

    struct OverlappedPayload {
        OVERLAPPED overlapped;
        DWORD numBytes;
        Event event;
        alias event this;
    }

    class EventQueue : EventQueueImpl!(HANDLE, OverlappedPayload*) {
    public:
        this() {
			sharedIOPortHandle = cast(shared void*)CreateIoCompletionPort(INVALID_HANDLE_VALUE, cast(HANDLE)0, cast(ULONG_PTR)0, 0);
			enforce(sharedIOPortHandle != INVALID_HANDLE_VALUE, "Error CreateIOCompletionPort " ~ to!string(GetLastError()));
            super(false, true);
        }

        OverlappedPayload* createQueueSpecificEvent(ref Event event) {
            auto op = makeNew(OverlappedPayload());
            op.event = event;
            op.overlapped.Offset = 0;
            op.overlapped.OffsetHigh = 0;
            protect(op);
            return op;
        }

        void triggerEvent(ref Event event) {
            auto op = createQueueSpecificEvent(event);
            auto overlapped = cast(OVERLAPPED*)op;
            enforce(PostQueuedCompletionStatus(ioPortHandle, 0, event.eventFilter, overlapped),
                "Error PostQueuedCompletionStatus " ~ to!string(GetLastError()));
        }

        @property HANDLE id() {
			return ioPortHandle;
        }

		override EventQueue clone() {
			return new EventQueue(ioPortHandle);
		}

    protected:

		this(HANDLE id) {
			this.sharedIOPortHandle = cast(shared void*)id;
			super(true, false);
		}

        void registerEventSource(T)(T handle) {
            enforce(CreateIoCompletionPort(cast(HANDLE)handle, ioPortHandle, 1, 0) == ioPortHandle,
                    "Error CreateIoCompletionPort " ~ to!string(GetLastError()));
        }
        
        void deregisterEventSource(T)(T handle) {
            // do nothing
        }

        override void runEventReader() {
            OverlappedPayload* overlapped;
            DWORD numBytes = 0;
            ULONG_PTR compKey = 0;
            /* If you are using GetQueuedCompletionStatus(), it will return FALSE if any failure occured.
             * If it was a socket failure, *lpOverlapped will be set to the non-NULL pointer value of the
             * OVERLAPPED operation that failed. If GetQueuedCompletionStatus() itself failed,
             * *lpOverlapped will be set to NULL. If the peer disconnects gracefully,
             * it will return TRUE and set *lpNumberOfBytes to 0 instead.
             */
            auto status = GetQueuedCompletionStatus(ioPortHandle, &numBytes, &compKey, cast(OVERLAPPED**)&overlapped, INFINITE);
            if(!status && overlapped !is null) {
                overlapped.event.flag = EventFlag.ERROR;
            } else if(!status) {
                throw new Exception("Error GetQueuedCompletionStatus " ~ to!string(GetLastError()));
            }
            overlapped.numBytes = numBytes;
            handleEvent(*overlapped);
        }
        
        override void shutdown() {
            CloseHandle(ioPortHandle);
        }

    private:
        shared HANDLE sharedIOPortHandle;

		@property HANDLE ioPortHandle() {
			return cast(HANDLE)sharedIOPortHandle;
		}
    }

    unittest {
        import devent.queuetester;
		auto factory = cast(shared) delegate() {
			return new EventQueue();
		};
        auto tester = new QueueTester!(HANDLE, OverlappedPayload*)(factory);
        tester.test();
    }
}

version(FreeBSD) {
    private import core.sys.freebsd.sys.event;
    private import core.stdc.errno;
    private import core.sys.posix.unistd;

    class EventQueue : EventQueueImpl!(int, kevent_t*) {

    public:
        this() {
            sharedHandle = cast(shared int)kqueue();
            enforce(sharedHandle > -1, "Failed to create kqueue");
            super(false, true);
        }

        this(int handle) {
            sharedHandle = cast(shared int)handle;
            super(true, false);
        }

        kevent_t* createQueueSpecificEvent(ref Event event) {
            protect(&event);
            auto voided = cast(void*)event;
            auto kevent = makeNew(kevent_t(1, cast(short)event.eventFilter, EV_ADD | EV_ENABLE, NOTE_FFNOP, 0, voided));
            protect(kevent);
            return kevent;
        }

        void triggerEvent(ref Event event) {
            assert(event.eventFilter == EventFilter.USER, "Can only trigger user defined events");
            auto change = createQueueSpecificEvent(event);
            change.flags |= EV_ONESHOT;
            auto post = new kevent_t(change.ident, change.filter, change.flags, change.fflags, 0, null);
            auto ret = kevent(mHandle, post, 1, null, 0, null);
            enforce(ret > -1, "Failed to queue event on kqueue: " ~ to!string(strerror(errno)));
            change.flags = 0;
            change.fflags = NOTE_TRIGGER;
            ret = kevent(mHandle, change, 1, null, 0, null);
            enforce(ret > -1, "Failed to trigger event on kqueue: " ~ to!string(strerror(errno)));
        }

        @property int id() {
            return mHandle;
        }

		override EventQueue clone() {
			return new EventQueue(mHandle);
		}
    protected:

        void registerEventSource(T)(T handle, ref Event event) {
            with(event) {
                assert(eventFilter != EventFilter.USER, "User defined event filters not allowed with file handles");
                auto change = createQueueSpecificEvent(event);
                change.ident = cast(int)handle;
                auto ret = kevent(mHandle, change, 1, null, 0, null);
                enforce(ret > -1, "Failed to set event in kqueue: " ~ to!string(strerror(errno)));
            }
        }
        
        void deregisterEventSource(T)(T handle, EventFilter deregisterForFilter) {
            auto del = kevent_t(cast(int)handle, cast(short)deregisterForFilter, EV_DELETE, 0, 0, null);
            auto ret = kevent(mHandle, &del, 1, null, 0, null);
            enforce(ret > -1, "Failed to remove handle " ~ to!string(handle) ~ " from kqueue: " ~ to!string(strerror(errno)));
        }

        override void runEventReader() {
            kevent_t kEvent;     // event that was triggered
            auto ret = kevent(mHandle, null, 0, &kEvent, 1, null);
            enforce(ret > -1, "Failed to read event from kqueue: " ~ to!string(strerror(errno)));
            auto event = cast(Event)kEvent.udata;
            if(kEvent.flags & EV_EOF) {
                event.flag(EventFlag.EOF);
            } else if (kEvent.flags && EVFILT_READ) {
                event.flag(EventFlag.READ);
            } else if (kEvent.flags && EVFILT_USER) {
                event.flag(EventFlag.USER);
            }
            handleEvent(event);
        }
        
        override void shutdown() {
            close(mHandle);
        }

    private:
        shared int sharedHandle = -1;

		@property int mHandle() {
			return cast(int) sharedHandle;
		}
    }

    unittest {
        import devent.queuetester;
        auto factory = cast(shared) delegate() {
			return new EventQueue();
        };
        auto tester = new QueueTester!(int, kevent_t*)(factory);
        tester.test();
    }

}

version(linux) {

    private import core.sys.linux.epoll;
    private import core.stdc.errno;
    private import utils.linuxapi;
    private import unistd = core.sys.posix.unistd;

	struct EpollEventWrapper {
		Event event;
		int handle = -1;
		alias event this;
	}

	class EventQueue : EventQueueImpl!(int, EpollEventWrapper*) {

    public:

        this() {
            sharedHandle = epoll_create(1);
			enforce(sharedHandle > -1, "Failed to create epoll: " ~ to!string(errno));
            super(false, true);
        }

        private this(int handle) {
            sharedHandle = cast(shared int) handle;
			isClone = true;
            super(true, false);
        }

		EpollEventWrapper* createQueueSpecificEvent(ref Event event) {
			auto ret = makeNew(EpollEventWrapper(event));
			protect(&event);
			return ret;
        }

        void triggerEvent(ref Event event) {
			uint count = 1;
			auto eew = createQueueSpecificEvent(event);
			int userEventSink = eventfd(0, EFD_NONBLOCK);
			eew.handle = userEventSink;
			auto ev = createEpollEvent(eew);
			auto ret = epoll_ctl(mHandle, EPOLL_CTL_ADD, userEventSink, ev);
			enforce(ret > -1, "Failed to set user defined epoll event: " ~ to!string(errno));
			enforce(unistd.write(userEventSink, &count, ulong.sizeof) == ulong.sizeof, "Sending user event failed");
        }

        @property int id() {
            return mHandle;
        }

		override EventQueue clone() {
			return new EventQueue(mHandle);
		}

    protected:

        void registerEventSource(T)(T handle, ref Event event) {
            assert(event.eventFilter != EventFilter.USER, "User defined event filters not allowed with file handles");
			auto eew = createQueueSpecificEvent(event);
			auto ev = createEpollEvent(eew);
			with(event) {
				auto ret = epoll_ctl(mHandle, EPOLL_CTL_ADD, handle, ev);
				enforce(ret > -1, "Failed to set epoll event: " ~ to!string(errno));
			}
		}
		
		void deregisterEventSource(T)(T handle, EventFilter deregisterForFilter) {
			auto ret = epoll_ctl(mHandle, EPOLL_CTL_DEL, handle, null);
			enforce(ret > -1, "Failed to remove epoll event: " ~ to!string(errno));
		}

        override void runEventReader() {
            epoll_event epEvent;     // event that was triggered
            auto ret = epoll_wait(mHandle, &epEvent, 1, -1);
            enforce(ret > -1, "Failed to epoll for event: " ~ to!string(errno));
            auto eventWrapper = cast(EpollEventWrapper*)epEvent.data.ptr;
			if(eventWrapper.handle != -1) {
				unistd.close(eventWrapper.handle);
			}
            handleEvent(eventWrapper.event);
        }
        
        override void shutdown() {
			if(!isClone) {
            	unistd.close(mHandle);
			}
        }

    private:
		shared int sharedHandle = -1;
		bool isClone = false;

		@property {
			int mHandle() {
				return cast(int) sharedHandle;
			}
		}

		epoll_event* createEpollEvent(EpollEventWrapper* eew) {
			auto voided = cast(void*)eew;
			auto data = epoll_data_t(voided);
            auto epollEvent = new epoll_event(eew.event.eventFilter | EPOLLET, data);
			//auto evFilter = (eew.event.eventFilter == EventFilter.USER ? EPOLLIN : eew.event.eventFilter) | epollEvent.events;
			//epollEvent.events = evFilter;
			return epollEvent;
		}
    }

    unittest {
        import devent.queuetester;
        auto factory = cast(shared) delegate() {
            return new EventQueue();
        };
		auto tester = new QueueTester!(int, EpollEventWrapper*)(factory);
        tester.test();
    }



}

version(Posix) {
    private import core.stdc.errno;
    @property int lastError() {
        return errno;
    }
}
