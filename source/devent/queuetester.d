module devent.queuetester;

version (unittest):
import std.concurrency, std.conv, std.stdio;
import core.thread;
import devent.ieventqueue;
import devent.event;

void spawner(void delegate() dp, Tid ownerThread) {
	try {
		dp();
	} catch(shared Exception exc) {
		ownerThread.send(exc);
	}
}

class TidEventHandler : EventHandler {
	void handle(ref Event myEvent) {
		auto tidName = myEvent.payload.get!string();
		auto tid = locate(tidName);
		send(tid, true);
	}
	void shutdown() {
	}
}

class DummyEventHandler : EventHandler {
    void handle(ref Event myEvent) {
        auto tidName = myEvent.payload.get!int();
    }
    void shutdown() {
    }
}

class QueueTester(HANDLETYPE, EVENTTYPE) {
	
	this(shared EventQueueImpl!(HANDLETYPE, EVENTTYPE) delegate() queueFactory) {
		this.subjectFactory = queueFactory;
		this.subject = queueFactory();
		auto handle = subject.id;
		log("Created queue with handle " ~ to!string(handle));
		auto dp = cast(shared){
			log("Listener started");
			receive(
				(bool wasSuccessful) { 
					assert(wasSuccessful);
					log("Listener was successful");
					log("Listener creating queue with handle " ~ to!string(handle));
					auto myQueue = subject.clone();
					myQueue.stopEventLoop();
					log("Listener stopped queue");
				}, 
				(shared(Throwable) exc) { throw exc; }
			);
		};
		testerTid = spawn(&spawner, dp, thisTid);
		register("listener", testerTid);
		EventHandler handler = new TidEventHandler;
		subject.registerHandler(handler);
	}
	
	void test() {
		auto handle = subject.id;
		auto delayTrigger = cast(shared){
			Variant str = "listener";
			auto s = new Event(str, EventFilter.USER);
			Thread.sleep(dur!("seconds")(5));
			log("Sender sending event on handle " ~ to!string(handle));
			auto myQueue = subject.clone();
			myQueue.triggerEvent(s);
			log("Sender sent event");
		};
		spawn(&spawner, delayTrigger, thisTid);
		log("Event loop started");
		subject.runEventLoop();
		log("Event loop complete");
	}
	
	void log(string msg) {
		writeln(msg);
		stdout.flush();
	}

private:
	shared EventQueueImpl!(HANDLETYPE, EVENTTYPE) delegate() subjectFactory;
	EventQueueImpl!(HANDLETYPE, EVENTTYPE) subject;
	Tid testerTid;
}
